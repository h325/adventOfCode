package solutions2022;

import java.util.LinkedList;

import fileManipulation.ReadFile;

public class solutionDay04 {

	public static String inputFileName = "day2022-04Input";

	public static void main(String[] args) {

		System.out.println("part 1:\n" + getSolutionPart1(ReadFile.getFileAsList(inputFileName)));
		System.out.println("part 2:\n" + getSolutionPart2(ReadFile.getFileAsList(inputFileName)));
	}

	public static int getSolutionPart1(LinkedList<String> input) {
		int counter = 0;
		for (String in : input) {
			int[] ranges = getRanges(in);

			boolean overlaps = ((ranges[0] <= ranges[2] && ranges[1] >= ranges[3])
					|| (ranges[0] >= ranges[2] && ranges[1] <= ranges[3]));

			if (overlaps)
				counter++;
		}
		return counter;
	}

	public static int getSolutionPart2(LinkedList<String> input) {
		int counter = 0;
		for (String in : input) {
			int[] ranges = getRanges(in);

			boolean overlaps = ((ranges[1] >= ranges[2] && ranges[3] >= ranges[1])
					|| (ranges[3] >= ranges[0] && ranges[1] >= ranges[3]));

			if (overlaps) {
				counter++;
				System.out.println(in + " overlaps");
			}

		}
		return counter;
	}

	static int[] getRanges(String in) {
		String[] sections = in.split(",");
		String[] sectionLeft = sections[0].split("-");
		String[] sectionRight = sections[1].split("-");
		int[] ranges = new int[] { Integer.parseInt(sectionLeft[0]), Integer.parseInt(sectionLeft[1]),
				Integer.parseInt(sectionRight[0]), Integer.parseInt(sectionRight[1]) };
		return ranges;
	}

}
