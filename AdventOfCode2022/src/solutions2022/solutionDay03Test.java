package solutions2022;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fileManipulation.ReadFile;

public class solutionDay03Test {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void getSolutionPart1Test() {
		int result = solutionDay03.getSolutionPart1(ReadFile.getFileAsList(solutionDay03.inputFileName));
		assertEquals(8176, result, 0);
	}
	
	@Test
	public void getSolutionPart2Test() {
		int result = solutionDay03.getSolutionPart2(ReadFile.getFileAsList(solutionDay03.inputFileName));
		assertEquals(2689, result, 0);
	}


}
