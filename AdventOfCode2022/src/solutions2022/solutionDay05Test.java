package solutions2022;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fileManipulation.ReadFile;

public class solutionDay05Test {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void getSolutionPart1Test() {
		String result = solutionDay05.getSolutionPart1(ReadFile.getFileAsList(solutionDay05.inputFileName));
		assertEquals("CMZ", result);
	}
	
	@Test
	public void getSolutionPart2Test() {
		String result = solutionDay05.getSolutionPart2(ReadFile.getFileAsList(solutionDay05.inputFileName));
		assertEquals("", result);
	}


}
