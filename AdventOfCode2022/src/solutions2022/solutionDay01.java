package solutions2022;

import java.util.Comparator;
import java.util.LinkedList;

import fileManipulation.ReadFile;

public class solutionDay01 {

	public static String inputFileName = "day2022-01Input";

	public static void main(String[] args) {
		System.out.println("part 1:\n" + getSolutionPart1(ReadFile.getFileAsList(inputFileName)));
		System.out.println("part 2:\n" + getSolutionPart2(ReadFile.getFileAsList(inputFileName)));
	}

	public static int getSolutionPart1(LinkedList<String> input) {
		return getMax(input);
	}

	public static int getSolutionPart2(LinkedList<String> input) {
		return getMax2(input);
	}

	private static int getMax(LinkedList<String> input) {
		// return input.stream().mapToInt(Integer::parseInt).sum();
		int max = 0;
		int localMax = 0;
		for (String item : input) {
			if (item == "") {
				localMax = 0;
			} else {
				localMax += Integer.parseInt(item);
				if (max < localMax)
					max = localMax;
			}
		}
		return max;
	}

	private static int getMax2(LinkedList<String> input) {
		LinkedList<Integer> totals = new LinkedList<Integer>();
		int localMax = 0;
		for (String item : input) {
			if (item == "") {
				totals.add(localMax);
				localMax = 0;
			} else {
				localMax += Integer.parseInt(item);
			}
		}
		totals.add(localMax);
		return totals.stream().sorted(Comparator.reverseOrder()).limit(3).mapToInt(Integer::intValue).sum();
	}

}
