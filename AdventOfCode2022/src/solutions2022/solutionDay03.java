package solutions2022;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import fileManipulation.ReadFile;

public class solutionDay03 {

	public static String inputFileName = "day2022-03Input";

	public static void main(String[] args) {

		System.out.println("part 1:\n" + getSolutionPart1(ReadFile.getFileAsList(inputFileName)));
		System.out.println("part 2:\n" + getSolutionPart2(ReadFile.getFileAsList(inputFileName)));
	}

	public static int getSolutionPart1(LinkedList<String> input) {
		int totalPriority = 0;
		for (String contents : input) {
			totalPriority += getPriority(new Rucksack(contents).bothCompartmentsContain());
		}
		return totalPriority;
	}

	public static int getSolutionPart2(LinkedList<String> input) {
		int totalPriority = 0;
		for(int i = 0; i < input.size(); i += 3) {
			totalPriority += getPriority(new RucksackGroup(input.subList(i, i+3)).getBadge());
		}
		return totalPriority;
	}

	private static int getPriority(String letter) {
		if (letter.length() > 1)
			throw new IllegalArgumentException("should be only one letter, but is " + letter);
		// uppercase
		if (65 <= letter.charAt(0) && letter.charAt(0) <= 90) {
			return (int) letter.charAt(0) - 64 + 26; // ASCII A = 65; priority of A = 27
		}
		// lowercase
		else if (97 <= letter.charAt(0) && letter.charAt(0) <= 122) {
			return (int) letter.charAt(0) - 96 + 0; // ASCII a = 97 priority a = 1
		} else {
			throw new IllegalArgumentException("cannot get priority of " + letter);
		}
	}

}

class Rucksack {
	String compartmentLeft;
	String compartmentRight;

	Rucksack(String contents) {
		compartmentLeft = contents.substring(0, contents.length() / 2);
		compartmentRight = contents.substring(contents.length() / 2);
	}

	public String bothCompartmentsContain() {
		for (int pos = 0; pos < compartmentLeft.length(); pos++) {
			if (compartmentRight.contains(compartmentLeft.subSequence(pos, pos + 1))) {
				return compartmentLeft.substring(pos, pos + 1);
			}
		}
		throw new IllegalArgumentException(
				"no common letter in compartments " + compartmentLeft + " and " + compartmentRight);
	}
}

class RucksackGroup {
	String[] rucksacks;

	RucksackGroup(List<String> rucksacks) {
		this.rucksacks = rucksacks.toArray(new String[0]);
	}

	String getBadge() {
		for (int pos = 0; pos < rucksacks[0].length(); pos++) {
			if (rucksacks[1].contains(rucksacks[0].subSequence(pos, pos + 1))
					&& rucksacks[2].contains(rucksacks[0].subSequence(pos, pos + 1))) {
				return rucksacks[0].substring(pos, pos + 1);
			}
		}
		throw new IllegalArgumentException(
				"no common letter common in " + Arrays.toString(rucksacks));

	}
}
