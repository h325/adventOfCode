package solutions2022;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import fileManipulation.ReadFile;

public class solutionDay05 {

	public static String inputFileName = "day2022-05Input";

	public static void main(String[] args) {
	

		System.out.println("part 1:\n" + getSolutionPart1(ReadFile.getFileAsList(inputFileName)));
		System.out.println("part 2:\n" + getSolutionPart2(ReadFile.getFileAsList(inputFileName)));
	}

	public static String getSolutionPart1(LinkedList<String> input) {
		
		int COLUMNS_COUNT = 9;
		int ARR_POS_MOVE_COUNT = 0;
		int ARR_POS_FROM = 1;
		int ARR_POS_TO = 2;

		LinkedList<LinkedList<String>> supplyStacks = new LinkedList<LinkedList<String>>();
		for(int i = 0; i < COLUMNS_COUNT; i++) {
			supplyStacks.add(new LinkedList<String>());
		}
		LinkedList<String[]> moves = new LinkedList<String[]>();
		
		int level = input.size() - 1;
		
		while(input.get(level).length()>0) {
			moves.addFirst(input.get(level).replaceAll("move ","").replaceAll("to ","").replaceAll("from ","").split(" "));
			level--;
		}
		
		//skip empty and line with numbers
		level-=2;
		
		for(; level >=0; level--) {
			String inputLine = input.get(level);
			//delete empty at the end
			for(int i = 1; i < inputLine.length(); i+=4) {
				String inputSymbol = inputLine.substring(i, i+1);
				int pos = (i-1)/4;
				if(!(inputSymbol.equals(" "))) supplyStacks.get(pos).add(inputSymbol);
			}
		}
		

		for(String[] move : moves) {
			
			LinkedList<String> stackFrom =  supplyStacks.get(Integer.parseInt(move[ARR_POS_FROM])-1);
			LinkedList<String> stackTo =  supplyStacks.get(Integer.parseInt(move[ARR_POS_TO])-1);
			for(int i = 0; i < Integer.parseInt(move[ARR_POS_MOVE_COUNT]); i++){
				stackTo.addLast(stackFrom.getLast()); 
				stackFrom.removeLast();
			}
			
		}
		
		String out = "";
		for(int col = 0; col < COLUMNS_COUNT; col++) {
			out = out + supplyStacks.get(col).getLast();
		}
		return out;
	}

	public static String getSolutionPart2(LinkedList<String> input) {

		int COLUMNS_COUNT = 9;
		int ARR_POS_MOVE_COUNT = 0;
		int ARR_POS_FROM = 1;
		int ARR_POS_TO = 2;

		LinkedList<LinkedList<String>> supplyStacks = new LinkedList<LinkedList<String>>();
		for(int i = 0; i < COLUMNS_COUNT; i++) {
			supplyStacks.add(new LinkedList<String>());
		}
		LinkedList<String[]> moves = new LinkedList<String[]>();
		
		int level = input.size() - 1;
		
		while(input.get(level).length()>0) {
			moves.addFirst(input.get(level).replaceAll("move ","").replaceAll("to ","").replaceAll("from ","").split(" "));
			level--;
		}
		
		//skip empty and line with numbers
		level-=2;
		
		for(; level >=0; level--) {
			String inputLine = input.get(level);
			//delete empty at the end
			for(int i = 1; i < inputLine.length(); i+=4) {
				String inputSymbol = inputLine.substring(i, i+1);
				int pos = (i-1)/4;
				if(!(inputSymbol.equals(" "))) supplyStacks.get(pos).add(inputSymbol);
			}
		}
		

		for(String[] move : moves) {
			
			LinkedList<String> stackFrom =  supplyStacks.get(Integer.parseInt(move[ARR_POS_FROM])-1);
			List<String> toBeMoved =  stackFrom.subList(stackFrom.size() - Integer.parseInt(move[ARR_POS_MOVE_COUNT]), stackFrom.size());
			LinkedList<String> stackTo =  supplyStacks.get(Integer.parseInt(move[ARR_POS_TO])-1);
			stackTo.addAll(toBeMoved);
			for(int i = 0; i < Integer.parseInt(move[ARR_POS_MOVE_COUNT]); i++){
				stackFrom.removeLast();
			}
			
		}
		
		String out = "";
		for(int col = 0; col < COLUMNS_COUNT; col++) {
			out = out + supplyStacks.get(col).getLast();
		}
		return out;
	}


}
