package solutions2022;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import fileManipulation.ReadFile;

public class solutionDay06 {

	public static String inputFileName = "day2022-06Input";

	public static void main(String[] args) {

		System.out.println("part 1:\n" + getSolutionPart1(ReadFile.getFileAsList(inputFileName)));
		System.out.println("part 2:\n" + getSolutionPart2(ReadFile.getFileAsList(inputFileName)));
	}

	public static int getSolutionPart1(LinkedList<String> input) {
		int pos;
		LinkedList<String> symbols = new LinkedList<String>();
		String datastream = input.get(0);
		for (pos = 0; pos < datastream.length(); pos++) {
			symbols.addLast(datastream.substring(pos, pos + 1));
			if (pos >= 4) {
				HashSet<String> unique = new HashSet<String>();
				List<String> lastFour = symbols.subList(pos - 4, pos);
				unique.addAll(lastFour);
				if (unique.size() == 4) {
					return pos;
				}
			}
		}
		throw new IllegalArgumentException("No sequence of 4 unique subsequent characters found");
	}

	public static int getSolutionPart2(LinkedList<String> input) {
		int pos;
		LinkedList<String> symbols = new LinkedList<String>();
		String datastream = input.get(0);
		for (pos = 0; pos < datastream.length(); pos++) {
			symbols.addLast(datastream.substring(pos, pos + 1));
			if (pos >= 14) {
				HashSet<String> unique = new HashSet<String>();
				List<String> lastFour = symbols.subList(pos - 14, pos);
				unique.addAll(lastFour);
				if (unique.size() == 14) {
					return pos;
				}
			}
		}
		throw new IllegalArgumentException("No sequence of 14 unique subsequent characters found");
	}

}
