package solutions2022;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import fileManipulation.ReadFile;

public class solutionDay02 {

	public static String inputFileName = "day2022-02Input";

	public static final Map<String, Integer> selectionValues;
	
	static {
		selectionValues = new HashMap<String, Integer>();
		selectionValues.put("X", 1);
		selectionValues.put("Y", 2);
		selectionValues.put("Z", 3);
	}

	public static void main(String[] args) {
	

		System.out.println("part 1:\n" + getSolutionPart1(ReadFile.getFileAsList(inputFileName)));
		System.out.println("part 2:\n" + getSolutionPart2(ReadFile.getFileAsList(inputFileName)));
	}

	public static int getSolutionPart1(LinkedList<String> input) {
		int score = 0;
		for (String round : input) {
			score += selectionValues.get(round.substring(2));
			score += getResultPart1(round);
		}
		return score;
	}

	public static int getSolutionPart2(LinkedList<String> input) {
		LinkedList<String> roundsPart2 = new LinkedList<String>();
		for (String round : input) {
			roundsPart2.add(getChoicesPart2(round));
		}
		return getSolutionPart1(roundsPart2);
	}

	private static String getChoicesPart2(String round) {
		
		String oponentChoice = round.substring(0, 1);
		String result = round.substring(2, 3);
		
		//X means you need to lose, 
		if(result.equals("X")) {
			if (oponentChoice.equals("A")) return oponentChoice + " " + "Z";
			if (oponentChoice.equals("B")) return oponentChoice + " " + "X";
			if (oponentChoice.equals("C")) return oponentChoice + " " + "Y";
		}
		
		//Y means you need to end the round in a draw, and
		if(result.equals("Y")) {
			return oponentChoice + " " + convertToXYZ(oponentChoice);
		}
		
		//Z means you need to win.
		if(result.equals("Z")) {
			if (oponentChoice.equals("A")) return oponentChoice + " " + "Y";
			if (oponentChoice.equals("B")) return oponentChoice + " " + "Z";
			if (oponentChoice.equals("C")) return oponentChoice + " " + "X";
		}
		
		throw new IndexOutOfBoundsException("Input " + round + " was not processed properly");
	}

	public static int getResultPart1(String round) {
		// equal
		String oponentChoice = round.substring(0, 1);
		String myChoice = round.substring(2, 3);
		//equal		
		if (oponentChoice.equals(convertToABC(myChoice)))
			return 3;
		// win
		if (oponentChoice.equals("A") && myChoice.equals("Y"))
			return 6;
		if (oponentChoice.equals("B") && myChoice.equals("Z"))
			return 6;
		if (oponentChoice.equals("C") && myChoice.equals("X"))
			return 6;
		//lose
		return 0;
	}
	
	public static String convertToABC(String xyz) {
		if(xyz.equals("X")) return "A";
		if(xyz.equals("Y")) return "B";
		if(xyz.equals("Z")) return "C";
		return null;
	}
	
	public static String convertToXYZ(String xyz) {
		if(xyz.equals("A")) return "X";
		if(xyz.equals("B")) return "Y";
		if(xyz.equals("C")) return "Z";
		return null;
	}
	
	

}
