package solutions2022;

import java.util.LinkedList;

import fileManipulation.ReadFile;

public class solutionDayXX {

	public static String inputFileName = "day2022-XXInput";

	public static void main(String[] args) {
	

		System.out.println("part 1:\n" + getSolutionPart1(ReadFile.getFileAsList(inputFileName)));
		System.out.println("part 2:\n" + getSolutionPart2(ReadFile.getFileAsList(inputFileName)));
	}

	public static int getSolutionPart1(LinkedList<String> input) {
		return -1;
	}

	public static int getSolutionPart2(LinkedList<String> input) {
		return -1;
	}


}
