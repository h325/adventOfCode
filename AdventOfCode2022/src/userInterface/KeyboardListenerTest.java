package userInterface;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class KeyboardListenerTest {

	@BeforeClass
	public static void setUpBeforeClass() {
	}

	@AfterClass
	public static void tearDownAfterClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown()  {
	}

	@Test
	public void testListen() throws Throwable {
		System.out.println("Zadejte 'ahoj'");
		String out = "";
		out = KeyboardListener.listen();
		assertEquals(out, "ahoj");
	}
	
	@Test
	public void testGetYE() throws Throwable {
		System.out.println("Are you human?");
		boolean answer = KeyboardListener.getYN();
		assertTrue(answer);
		System.out.println("Are you mutant?");
		answer = KeyboardListener.getYN();
		assertFalse(answer);
		System.out.println("Please make some invalid choice");
		answer = KeyboardListener.getYN();
		assertFalse(answer);
	}
	

}
