package userInterface;

import java.util.Scanner;

public final class KeyboardListener {

    private static Scanner scanner;

	public static String listen() throws Throwable {
        scanner = new Scanner(System.in);
        
        String userInput = "";
        System.out.printf("Waiting for input:\n");
        userInput = scanner.nextLine();
        return userInput;
    }
	
	public static boolean getYN() throws Throwable {
		System.out.println("Select yes (Y) or no (N)");
		String in = listen();
		if(in.equals("Y")) {
			return true;
		}else if(!in.equals("N")) {
			System.out.println("Invalid choice, considered as NO\n");
		}
		return false;
	}
	
	public static boolean getYN(String message) throws Throwable {
		System.out.println(message);
		return getYN();
	}
	
}
