package fileManipulation;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class WriteFileTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testWriteIntoFile() {
		String testfileName = "testOutput.txt";
		new File(testfileName).delete();

		System.out.println("test: create new file testOutput.txt without user interaction");
		String[] testInput1 = { "0", "a", "xyz", "", "��dek 5" };
		WriteFile.writeIntoFile(testfileName, testInput1);
		LinkedList<String> expected = new LinkedList<String>();
		expected.addAll(Arrays.asList(testInput1));
		assertEquals(expected, ReadFile.getFileAsList(testfileName));

		System.out.println("test: append file testOutput.txt");
		String[] testInput2 = { "sd dsfa asdf", "��dek 7" };
		WriteFile.writeIntoFile(testfileName, testInput2);
		expected.addAll(Arrays.asList(testInput2));
		assertEquals(expected, ReadFile.getFileAsList(testfileName));

		System.out.println("test: overwrite file testOutput.txt");
		WriteFile.writeIntoFile(testfileName, testInput1);
		expected.clear();
		expected.addAll(Arrays.asList(testInput1));
		assertEquals(expected, ReadFile.getFileAsList(testfileName));

		System.out.println("test: save as file testOutput.txt with timestamp");
		File fileOut = WriteFile.writeIntoFile(testfileName, testInput2);
		expected.clear();
		expected.addAll(Arrays.asList(testInput2));
		assertEquals(expected, ReadFile.getFileAsList(fileOut.getPath()));

		System.out.println("test: cancel export");
		fileOut = WriteFile.writeIntoFile(testfileName, testInput1);
		expected = null;
		assertEquals(null, fileOut);
		
		String testFileWOExtension = "testFile";
		try {
			new File(testFileWOExtension).createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("test: save as file with timestamp");
		fileOut = WriteFile.writeIntoFile(testFileWOExtension, testInput1);
		expected = new LinkedList<String>();
		expected.addAll(Arrays.asList(testInput1));
		assertEquals(expected, ReadFile.getFileAsList(fileOut.getPath()));
		
	}

}
